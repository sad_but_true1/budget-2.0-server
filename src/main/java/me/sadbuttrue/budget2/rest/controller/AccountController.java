package me.sadbuttrue.budget2.rest.controller;

import me.sadbuttrue.budget2.model.Account;
import me.sadbuttrue.budget2.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by true on 04/07/15.
 */
@RestController(value = "/accounts")
public class AccountController {
    @Autowired
    private AccountService accountService;

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public ResponseEntity<Account> findById(@PathVariable("id") final long id) {
        Account account = accountService.getById(id);
        return new ResponseEntity<Account>(account, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/delete{id}")
    public String deleteById(@PathVariable("id") final long id) {
        accountService.delete(accountService.getById(id));
        return "Done";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/helloaccount")
    public ResponseEntity<Account> get(@RequestBody Account account) {
        return new ResponseEntity<Account>(account, HttpStatus.OK);
    }
}
