package me.sadbuttrue.budget2.model;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by true on 19/06/15.
 */
@javax.persistence.Entity
@Table
@Inheritance(strategy = InheritanceType.JOINED)
public class Entity {
    @Column(nullable = false)
    protected String name;
    @Id
    @GeneratedValue
    protected long id;
    @Column
    protected Date creationDate;
    @Column
    protected String additionalInfo;

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
