package me.sadbuttrue.budget2.model;

import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

/**
 * Created by true on 19/06/15.
 */
@javax.persistence.Entity
@Table
@PrimaryKeyJoinColumn(name="ID")
public class PayeeType extends Entity {
    @ManyToOne(optional = false)
    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
