package me.sadbuttrue.budget2.model;

import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import java.util.Date;

/**
 * Created by true on 19/06/15.
 */
@javax.persistence.Entity
@Table
@PrimaryKeyJoinColumn(name = "ID")
public class Payment extends Entity {
    @ManyToOne(optional = false)
    private Account account;
    @ManyToOne
    private Payee payee;
    @Column
    private boolean cleared;
    @Column
    private Date clearedDate;

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Payee getPayee() {
        return payee;
    }

    public void setPayee(Payee payee) {
        this.payee = payee;
    }

    public boolean isCleared() {
        return cleared;
    }

    public void setCleared(boolean cleared) {
        this.cleared = cleared;
    }

    public Date getClearedDate() {
        return clearedDate;
    }

    public void setClearedDate(Date clearedDate) {
        this.clearedDate = clearedDate;
    }
}
