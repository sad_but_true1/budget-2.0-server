package me.sadbuttrue.budget2.model;

import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

/**
 * Created by true on 19/06/15.
 */
@javax.persistence.Entity
@Table
@PrimaryKeyJoinColumn(name="ID")
public class Payee extends Entity {
    @ManyToOne(optional = false)
    private PayeeType type;

    public PayeeType getType() {
        return type;
    }

    public void setType(PayeeType type) {
        this.type = type;
    }
}
