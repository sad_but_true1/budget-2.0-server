package me.sadbuttrue.budget2.dao.impl;

import me.sadbuttrue.budget2.dao.GenericDAO;
import me.sadbuttrue.budget2.util.SessionHelper;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;

/**
 * Created by true on 19/06/15.
 */
@Repository
@Transactional
public class GenericDAOImpl<T, ID extends Serializable> implements GenericDAO<T, ID> {
    @Autowired
    private SessionHelper sessionHelper;

    @Override
    public void save(T entity) {
        Session hibernateSession = sessionHelper.getCurrentSession();
        hibernateSession.saveOrUpdate(entity);
    }

    @Override
    public void merge(T entity) {
        Session hibernateSession = sessionHelper.getCurrentSession();
        hibernateSession.merge(entity);
    }

    @Override
    public void delete(T entity) {
        Session hibernateSession = sessionHelper.getCurrentSession();
        hibernateSession.delete(entity);
    }

    @Override
    public List<T> findMany(Query query) {
        List<T> t;
        t = (List<T>) query.list();
        return t;
    }

    @Override
    public T findOne(Query query) {
        T t;
        t = (T) query.uniqueResult();
        return t;
    }

    @Override
    public List findAll(Class clazz) {
        Session hibernateSession = sessionHelper.getCurrentSession();
        List T = null;
        Query query = hibernateSession.createQuery("from " + clazz.getName());
        T = query.list();
        return T;
    }

    @Override
    public T findByID(Class clazz, ID id) {
        Session hibernateSession = sessionHelper.getCurrentSession();
        T t = null;
        t = (T) hibernateSession.get(clazz, id);
        return t;
    }
}
