package me.sadbuttrue.budget2.dao;

import org.hibernate.Query;

import java.io.Serializable;
import java.util.List;

/**
 * Created by true on 19/06/15.
 */
public interface GenericDAO<T, ID extends Serializable> {
    void save(T entity);

    void merge(T entity);

    void delete(T entity);

    List<T> findMany(Query query);

    T findOne(Query query);

    List findAll(Class clazz);

    T findByID(Class clazz, ID id);
}
