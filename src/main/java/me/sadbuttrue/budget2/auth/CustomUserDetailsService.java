package me.sadbuttrue.budget2.auth;

import me.sadbuttrue.budget2.model.User;
import me.sadbuttrue.budget2.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

//import org.springframework.security.core.userdetails.User;

/**
 * Created by true on 03/07/15.
 */
@Service
@Transactional(readOnly = true)
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    private UserService userService;

    public static List<GrantedAuthority> getGrantedAuthorities(List<String> roles) {
        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();

        for (String role : roles) {
            authorities.add(new SimpleGrantedAuthority(role));
        }
        return authorities;
    }

    @Override
    public UserDetails loadUserByUsername(String login)
            throws UsernameNotFoundException {

        User user = userService.getByName(login);
        if (user == null) {
            throw new UsernameNotFoundException(login);
        }

        List<GrantedAuthority> grantedAuthorities = new ArrayList<GrantedAuthority>();
        List<String> permissions = userService.getPermissions(user.getName());
        for (String permission : permissions) {
            grantedAuthorities.add(new SimpleGrantedAuthority(permission));
        }

        return new CustomUserDetails(user, grantedAuthorities);
//        me.sadbuttrue.budget2.model.User domainUser = userService.getByName(login);
//
//        boolean enabled = true;
//        boolean accountNonExpired = true;
//        boolean credentialsNonExpired = true;
//        boolean accountNonLocked = true;
//
//        return new User(
//                domainUser.getName(),
//                domainUser.getPassword(),
//                enabled,
//                accountNonExpired,
//                credentialsNonExpired,
//                accountNonLocked,
//                getAuthorities(1)
//        );
    }

    public Collection<? extends GrantedAuthority> getAuthorities(Integer role) {
        List<GrantedAuthority> authList = getGrantedAuthorities(getRoles(role));
        return authList;
    }

    public List<String> getRoles(Integer role) {

        return Collections.singletonList("ROLE_USER");
//        List<String> roles = new ArrayList<String>();
//
//        if (role.intValue() == 1) {
//            roles.add("ROLE_MODERATOR");
//            roles.add("ROLE_ADMIN");
//        } else if (role.intValue() == 2) {
//            roles.add("ROLE_MODERATOR");
//        }
//        return roles;
    }

}