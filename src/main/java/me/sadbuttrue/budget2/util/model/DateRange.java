package me.sadbuttrue.budget2.util.model;

import java.util.Date;

/**
 * Created by true on 25/06/15.
 */
public class DateRange {
    private Date begin;
    private Date end;

    public DateRange(Date begin, Date end) {

        this.begin = begin;
        this.end = end;
    }

    public Date getBegin() {
        return begin;
    }

    public Date getEnd() {
        return end;
    }
}
