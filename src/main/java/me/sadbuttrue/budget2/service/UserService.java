package me.sadbuttrue.budget2.service;

import me.sadbuttrue.budget2.model.User;

import java.util.List;

/**
 * Created by true on 20/06/15.
 */
public interface UserService {
    User getByEmail(String email);

    User getByName(String name);

    User getById(Long id);

    List<User> getAll();

    void save(User user);

    void delete(User user);

    void merge(User user);

    Boolean isCurrentUserLoggedIn();

    List<String> getPermissions(String username);
}
