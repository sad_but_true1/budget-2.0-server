package me.sadbuttrue.budget2.service;

import me.sadbuttrue.budget2.model.Account;
import me.sadbuttrue.budget2.model.User;

import java.util.List;

/**
 * Created by true on 25/06/15.
 */
public interface AccountService {
    List<Account> getByUser(User user);

    Account getById(Long id);

    List<Account> getAll();

    void save(Account account);

    void delete(Account account);

    void merge(Account account);
}
