package me.sadbuttrue.budget2.service;

import me.sadbuttrue.budget2.model.PayeeType;
import me.sadbuttrue.budget2.model.User;

import java.util.List;

/**
 * Created by true on 25/06/15.
 */
public interface PayeeTypeService {
    List<PayeeType> getByUser(User user);

    PayeeType getById(Long id);

    List<PayeeType> getAll();

    void save(PayeeType type);

    void delete(PayeeType type);

    void merge(PayeeType type);
}
