package me.sadbuttrue.budget2.service;

import me.sadbuttrue.budget2.model.*;
import me.sadbuttrue.budget2.util.model.DateRange;

import java.util.List;

/**
 * Created by true on 25/06/15.
 */
public interface PaymentService {
    List<Payment> getByPayee(Payee payee);

    List<Payment> getByPayeeType(PayeeType type);

    List<Payment> getByUser(User user);

    List<Payment> getByAccount(Account account);

    List<Payment> getByDateRange(DateRange range);

    Payment getById(Long id);

    List<Payment> getAll();

    void save(Payment payment);

    void delete(Payment payment);

    void merge(Payment payment);
}
