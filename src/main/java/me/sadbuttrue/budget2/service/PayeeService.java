package me.sadbuttrue.budget2.service;

import me.sadbuttrue.budget2.model.Payee;
import me.sadbuttrue.budget2.model.PayeeType;
import me.sadbuttrue.budget2.model.User;

import java.util.List;

/**
 * Created by true on 25/06/15.
 */
public interface PayeeService {
    List<Payee> getByType(PayeeType type);

    List<Payee> getByUser(User user);

    Payee getById(Long id);

    List<Payee> getAll();

    void save(Payee payee);

    void delete(Payee payee);

    void merge(Payee payee);
}
