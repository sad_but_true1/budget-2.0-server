package me.sadbuttrue.budget2.service.impl;

import me.sadbuttrue.budget2.dao.GenericDAO;
import me.sadbuttrue.budget2.model.Payee;
import me.sadbuttrue.budget2.model.PayeeType;
import me.sadbuttrue.budget2.model.User;
import me.sadbuttrue.budget2.service.PayeeService;
import me.sadbuttrue.budget2.util.SessionHelper;
import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * Created by true on 25/06/15.
 */
@Service
@Transactional
public class PayeeServiceImpl implements PayeeService {
    @Autowired
    private GenericDAO<Payee, Long> dao;
    @Autowired
    private SessionHelper sessionHelper;

    @Override
    public List<Payee> getByType(PayeeType type) {
        Query query = sessionHelper.getCurrentSession().createQuery("from Payee p where p.type = :type");
        query.setParameter("type", type);
        return dao.findMany(query);
    }

    @Override
    public List<Payee> getByUser(User user) {
        Query query = sessionHelper.getCurrentSession().createQuery("from Payee p where p.type in " +
                "(from PayeeType pt where pt.user = :user)");
        query.setParameter("user", user);
        return dao.findMany(query);
    }

    @Override
    public Payee getById(Long id) {
        return dao.findByID(Payee.class, id);
    }

    @Override
    public List<Payee> getAll() {
        return dao.findAll(Payee.class);
    }

    @Override
    public void save(Payee payee) {
        payee.setCreationDate(new Date());
        dao.save(payee);
    }

    @Override
    public void delete(Payee payee) {
        dao.delete(payee);
    }

    @Override
    public void merge(Payee payee) {
        dao.merge(payee);
    }
}
