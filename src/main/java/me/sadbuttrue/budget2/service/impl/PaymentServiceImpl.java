package me.sadbuttrue.budget2.service.impl;

import me.sadbuttrue.budget2.dao.GenericDAO;
import me.sadbuttrue.budget2.model.*;
import me.sadbuttrue.budget2.service.PaymentService;
import me.sadbuttrue.budget2.util.SessionHelper;
import me.sadbuttrue.budget2.util.model.DateRange;
import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * Created by true on 25/06/15.
 */
@Service
@Transactional
public class PaymentServiceImpl implements PaymentService {
    @Autowired
    private GenericDAO<Payment, Long> dao;
    @Autowired
    private SessionHelper sessionHelper;

    @Override
    public List<Payment> getByPayee(Payee payee) {
        Query query = sessionHelper.getCurrentSession().createQuery("from Payment p where p.payee = :payee");
        query.setParameter("payee", payee);
        return dao.findMany(query);
    }

    @Override
    public List<Payment> getByPayeeType(PayeeType type) {
        Query query = sessionHelper.getCurrentSession().createQuery("from Payment p where p.payee in " +
                "(from Payee pa where pa.type = :type)");
        query.setParameter("type", type);
        return dao.findMany(query);
    }

    @Override
    public List<Payment> getByUser(User user) {
        Query query = sessionHelper.getCurrentSession().createQuery("from Payment p where p.payee.type.user = :user or " +
                "p.account.user = :user");
        query.setParameter("user", user);
        return dao.findMany(query);
    }

    @Override
    public List<Payment> getByAccount(Account account) {
        Query query = sessionHelper.getCurrentSession().createQuery("from Payment p where p.account = :account");
        query.setParameter("account", account);
        return dao.findMany(query);
    }

    @Override
    public List<Payment> getByDateRange(DateRange range) {
        Query query = sessionHelper.getCurrentSession().createQuery("from Payment p where p.creationDate >= :start and " +
                "p.creationDate <= :stop");
        query.setParameter("start", range.getBegin());
        query.setParameter("stop", range.getEnd());
        return dao.findMany(query);
    }

    @Override
    public Payment getById(Long id) {
        return dao.findByID(Payment.class, id);
    }

    @Override
    public List<Payment> getAll() {
        return dao.findAll(Payment.class);
    }

    @Override
    public void save(Payment payment) {
        payment.setCreationDate(new Date());
        dao.save(payment);
    }

    @Override
    public void delete(Payment payment) {
        dao.delete(payment);
    }

    @Override
    public void merge(Payment payment) {
        dao.merge(payment);
    }
}
