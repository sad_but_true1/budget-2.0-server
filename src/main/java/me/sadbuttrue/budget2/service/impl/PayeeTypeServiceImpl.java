package me.sadbuttrue.budget2.service.impl;

import me.sadbuttrue.budget2.dao.GenericDAO;
import me.sadbuttrue.budget2.model.PayeeType;
import me.sadbuttrue.budget2.model.User;
import me.sadbuttrue.budget2.service.PayeeTypeService;
import me.sadbuttrue.budget2.util.SessionHelper;
import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * Created by true on 25/06/15.
 */
@Service
@Transactional
public class PayeeTypeServiceImpl implements PayeeTypeService {
    @Autowired
    private GenericDAO<PayeeType, Long> dao;
    @Autowired
    private SessionHelper sessionHelper;

    @Override
    public List<PayeeType> getByUser(User user) {
        Query query = sessionHelper.getCurrentSession().createQuery("from PayeeType pt where pt.user = :user");
        query.setParameter("user", user);
        return dao.findMany(query);
    }

    @Override
    public PayeeType getById(Long id) {
        return dao.findByID(PayeeType.class, id);
    }

    @Override
    public List<PayeeType> getAll() {
        return dao.findAll(PayeeType.class);
    }

    @Override
    public void save(PayeeType type) {
        type.setCreationDate(new Date());
        dao.save(type);
    }

    @Override
    public void delete(PayeeType type) {
        dao.delete(type);
    }

    @Override
    public void merge(PayeeType type) {
        dao.merge(type);
    }
}
