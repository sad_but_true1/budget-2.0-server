package me.sadbuttrue.budget2.service.impl;

import me.sadbuttrue.budget2.auth.LoggedInChecker;
import me.sadbuttrue.budget2.dao.GenericDAO;
import me.sadbuttrue.budget2.model.User;
import me.sadbuttrue.budget2.service.UserService;
import me.sadbuttrue.budget2.util.SessionHelper;
import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by true on 20/06/15.
 */
@Service
@Transactional
public class UserServiceImpl implements UserService {
    @Autowired
    private GenericDAO<User, Long> dao;

    @Autowired
    private SessionHelper sessionHelper;

    @Autowired
    private LoggedInChecker loggedInChecker;

    @Override
    public User getByEmail(String email) {
        Query query = sessionHelper.getCurrentSession().createQuery("from User u where u.email = :email");
        query.setParameter("email", email);
        return dao.findOne(query);
    }

    @Override
    public User getByName(String name) {
        Query query = sessionHelper.getCurrentSession().createQuery("from User u where u.name = :name");
        query.setParameter("name", name);
        return dao.findOne(query);
    }

    @Override
    public User getById(Long id) {
        return dao.findByID(User.class, id);
    }

    @Override
    public List<User> getAll() {
        return dao.findAll(User.class);
    }

    @Override
    public void save(User user) {
        user.setCreationDate(new Date());
        dao.save(user);
    }

    @Override
    public void delete(User user) {
        dao.delete(user);
    }

    @Override
    public void merge(User user) {
        dao.merge(user);
    }

    @Override
    public Boolean isCurrentUserLoggedIn() {
        return loggedInChecker.getLoggedInUser() != null;
    }

    @Override
    public List<String> getPermissions(String username) {
        return new ArrayList<String>();
    }
}
