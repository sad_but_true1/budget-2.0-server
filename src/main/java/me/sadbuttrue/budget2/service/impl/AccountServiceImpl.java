package me.sadbuttrue.budget2.service.impl;

import me.sadbuttrue.budget2.dao.GenericDAO;
import me.sadbuttrue.budget2.model.Account;
import me.sadbuttrue.budget2.model.User;
import me.sadbuttrue.budget2.service.AccountService;
import me.sadbuttrue.budget2.util.SessionHelper;
import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * Created by true on 25/06/15.
 */
@Service
@Transactional
public class AccountServiceImpl implements AccountService {
    @Autowired
    private GenericDAO<Account, Long> dao;

    @Autowired
    private SessionHelper sessionHelper;

    @Override
    public List<Account> getByUser(User user) {
        Query query = sessionHelper.getCurrentSession().createQuery("from Account a where a.user = :user");
        query.setParameter("user", user);
        return dao.findMany(query);
    }

    @Override
    public Account getById(Long id) {
        return dao.findByID(Account.class, id);
    }

    @Override
    public List<Account> getAll() {
        return dao.findAll(Account.class);
    }

    @Override
    public void save(Account account) {
        account.setCreationDate(new Date());
        dao.save(account);
    }

    @Override
    public void delete(Account account) {
        dao.delete(account);
    }

    @Override
    public void merge(Account account) {
        dao.merge(account);
    }
}
